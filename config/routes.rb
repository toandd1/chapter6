Rails.application.routes.draw do
  get 'sessions/new'
  root 'static_pages#home'
  get '/help', to: 'static_pages#help', as: 'helf'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'
  get '/users:id', to: 'users#show'
  get '/users/new', to: 'users#new'
  get '/users', to: 'users#index'
  post '/users', to: 'users#create'
  get '/users/:id/edit', to: 'users#edit'
  put '/users/:id', to: 'users#new'
  delete '/users:id', to: 'users#new'
  get     "/login"    => "sessions#new"
  post    "/login"    => "sessions#create"
  delete  "/logout"   => "sessions#destroy"
  resources :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

end
